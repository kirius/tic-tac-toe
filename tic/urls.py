# coding=utf-8
from django.conf.urls import patterns, include, url
from django.contrib import admin
import socketio.sdjango


socketio.sdjango.autodiscover()


urlpatterns = patterns(
    '',
    # Examples:
    # url(r'^$', 'tic.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^jsi18n/$', 'django.views.i18n.javascript_catalog'),
    url(r'^admin/', include(admin.site.urls)),
    url("^socket\.io", include(socketio.sdjango.urls)),
    url(r'^auth/', include("auth.urls")),
    url(r'^lang/(?P<lang>\w+)$', 'game.views.change_lang', name='change_lang'),
    url("^$", "game.views.players", name="players"),
)
