# coding=utf-8
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
from django.contrib.auth.views import logout_then_login
from game.utils import remove_from_online


def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            new_user = authenticate(username=form.cleaned_data['username'],
                                    password=form.cleaned_data['password1'])
            login(request, new_user)
            return redirect('players')
        else:
            return render(request, 'auth/register.html', {'form': form})

    form = UserCreationForm()
    return render(request, 'auth/register.html', {'form': form})


def logout(request):
    remove_from_online(request.user)
    return logout_then_login(request)
