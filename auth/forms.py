# coding=utf-8
from django.contrib.sessions.models import Session
from django.forms import ValidationError
from django.contrib.auth.forms import AuthenticationForm
from django.utils.translation import ugettext_lazy as _
# from game.utils import get_online_users


class SingleLoginForm(AuthenticationForm):
    def confirm_login_allowed(self, user):
        super(SingleLoginForm, self).confirm_login_allowed(user)
        for s in Session.objects.all():
            if s.get_decoded().get('_auth_user_id') == user.id:
                raise ValidationError(
                    _('You already logged in from another place'),
                    code='inactive',
                )

        # if str(user.pk) in get_online_users():
        #     raise ValidationError(
        #         _('You already logged in from another place'),
        #         code='inactive',
        #     )
