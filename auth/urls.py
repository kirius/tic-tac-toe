# coding=utf-8
from django.conf.urls import patterns, url
from auth.forms import SingleLoginForm


urlpatterns = patterns(
    'django.contrib.auth.views',
    url(r'^login/$', 'login', {'template_name': 'auth/login.html',
                               'authentication_form': SingleLoginForm},
        name='login'),
)

urlpatterns += patterns(
    'auth.views',
    url(r'^register/$', 'register', name='register'),
    url(r'^logout/$', 'logout', name='logout'),
)
