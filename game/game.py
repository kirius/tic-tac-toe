# coding=utf-8
from itertools import chain
import copy


class Board(object):
    """
    Represents a board for the game.
    """
    empty_mark = ' '

    def __init__(self, size=3, init_board=None):

        self.size = size
        if init_board is None:
            self.board = [[self.empty_mark] * size for _ in range(size)]
        else:
            self.board = init_board

    def __eq__(self, other):
        return self.board == other.board

    def __hash__(self):
        return hash(str(self.board))

    def __str__(self):
        return '\n' + '\n'.join(str(line) for line in self.board) + '\n'

    def copy(self):
        return Board(self.size, copy.deepcopy(self.board))

    def check_move(self, x, y):
        """
        Returns True if the move is valid
        and False otherwise.
        """
        try:
            if x < 0 or y < 0:
                # accept only positive coordinates
                return False
            return self.board[x][y] == self.empty_mark
        except (TypeError, IndexError):
            return False

    def move(self, x, y, mark):
        """
        Tries to perform move.
        Mutates self and returns it on success.
        Returns False on failure.
        """
        if self.check_move(x, y):
            self.board[x][y] = mark
            return self
        return False

    def get_all_moves(self):
        """
        Returns list of all valid moves (ex. [(1,2), (2,3)])
        """
        size = range(self.size)
        return [(x, y) for x in size for y in size
                if self.board[x][y] == self.empty_mark]

    def won(self):
        """
        Returns True if any player won.
        (filled horizontal, vertical or diagonal lines).
        """
        size = range(self.size)
        # chain horizontal, vertical and diagonal lines in 1 iterable
        lines = chain(self.board,
                      zip(*self.board),
                      [[self.board[i][j] for i in size for j in size if i == j]],
                      [[self.board[i][self.size - j - 1] for i in size for j in size if i == j]])
        # returns True if there is a line which completely filled in by one of the players
        return any(line[0] != self.empty_mark
                   and line.count(line[0]) == self.size for line in lines)

    def finished(self):
        """
        Checks if a game has finished.
        Returns 1 if someone won, 2 if a draw, 0 if game continues
        """
        if self.won():
            return 1
        if any(cell == self.empty_mark for row in self.board for cell in row):
            # empty cell(s) is present
            return 0
        # nobody won and no empty cells left => draw
        return 2


class Game(object):
    def __init__(self, player1, player2, id):
        self.id = id
        self.next = {player1: player2, player2: player1}
        self.marks = {player1: 'x', player2: 'o'}
        self.board = Board()
        self.turn = player1

    def move(self, x, y, player):
        x = int(x)
        y = int(y)
        if self.check_permission(player):
            if self.board.move(x, y, self.marks[player]):
                self.turn = self.next[player]
                return True
        return False

    def check_permission(self, player):
        return self.turn == player

    def cur_to_dict(self):
        return {'id': self.turn, 'mark': self.marks[self.turn]}

    def opp_to_dict(self):
        opp = self.next[self.turn]
        return {'id': opp, 'mark': self.marks[opp]}
