# coding=utf-8
import redis
import pickle
from django.conf import settings

import json
from .game import Game


REDIS_SETTINGS = getattr(settings, 'REDIS_SETTINGS')
REDIS_POOL = redis.ConnectionPool(
    host=REDIS_SETTINGS['HOST'],
    port=REDIS_SETTINGS['PORT'],
    db=REDIS_SETTINGS['DB']
)


def redis_connection():
    """
    Returns a redis connection.
    """
    return redis.StrictRedis(connection_pool=REDIS_POOL)


def add_to_online(user):
    conn = redis_connection()
    if not conn.hexists('online_users', user.id):
        conn.hset('online_users', user.id, user.username)
    emit_all_but_me(me=user, event='user_online',
                    msg={'id': user.id, 'name': user.username})


def remove_from_online(user):
    conn = redis_connection()
    if conn.hexists('online_users', user.id):
        conn.hdel('online_users', user.id)
        emit_all_but_me(me=user, event='user_offline',
                        msg={'id': user.id, 'name': user.username})


def get_online_users():
    conn = redis_connection()
    return conn.hkeys('online_users')


def emit(channel, event, msg, conn=None):
    if conn is None:
        conn = redis_connection()

    conn.publish('socketio_%s' % channel,
                 json.dumps({'event': event, 'msg': msg}))


def emit_all_but_me(me, event, msg):
    conn = redis_connection()
    user_id = str(me.id)
    users = get_online_users()
    for user in users:
        if user_id != user:
            emit(channel=user, event=event, msg=msg, conn=conn)


def create_game(user1, user2):
    conn = redis_connection()
    game_id = conn.incr('game_seq')
    game = Game(user1, user2, game_id)
    conn.set('game_%s' % game_id, pickle.dumps(game))
    return game


def get_game(game_id):
    game = pickle.loads(redis_connection().get('game_%s' % game_id))
    return game


def save_game(game):
    redis_connection().set('game_%s' % game.id, pickle.dumps(game))
