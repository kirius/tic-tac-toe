# coding=utf-8
import json

from socketio.namespace import BaseNamespace
from socketio.sdjango import namespace

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.conf import settings

from .utils import redis_connection, emit, add_to_online, \
    remove_from_online, get_online_users, create_game, get_game, save_game


@namespace('/game')
class MyNamespace(BaseNamespace):
    def listener(self):
        r = redis_connection().pubsub()
        r.subscribe('socketio_%s' % self.user.id)

        for m in r.listen():
            if m['type'] == 'message':
                data = json.loads(m['data'])
                self.emit(data['event'], data['msg'])

    def on_subscribe(self, *args):
        self.user = self.request.user
        self.spawn(self.listener)

    def on_online(self, *args):
        add_to_online(self.user)

    def recv_disconnect(self):
        remove_from_online(self.user)
        self.disconnect()

    def on_invite(self, user):
        emit(user, 'invite', {'id': self.user.id, 'name': self.user.username})

    def on_accept_invite(self, user):
        game = create_game(self.user.id, user)
        emit(self.user.id, 'game_start',
             {'id': game.id,
              'me': game.cur_to_dict(),
              'opp': game.opp_to_dict()
              })

        emit(user, 'game_start',
             {'id': game.id,
              'me': game.opp_to_dict(),
              'opp': game.cur_to_dict()
              })

        emit(self.user.id, 'move', None)

    def on_decline_invite(self, user, reason):
        emit(user, 'decline_invite',
             {'id': self.user.id, 'name': self.user.username,
              'reason': reason})

    def on_cancel_invite(self, user):
        emit(user, 'cancel_invite', {})

    def on_replay(self, user):
        emit(user, 'replay', {'id': self.user.id, 'name': self.user.username})

    def on_decline_replay(self, user):
        emit(user, 'decline_replay', {'id': self.user.id,
                                      'name': self.user.username})

    def on_move(self, move, game_id):
        game = get_game(game_id)
        if game.move(move['x'], move['y'], self.user.id):
            save_game(game)
            res = game.board.finished()
            emit(game.turn, 'move', move)
            if res == 1:
                emit(self.user.id, 'game_end', 'won')
                emit(game.turn, 'game_end', 'lost')
            elif res == 2:
                emit(self.user.id, 'game_end', 'draw')
                emit(game.turn, 'game_end', 'draw')

    def on_stop_game(self, user):
        emit(user, 'stop_game',
             {'id': self.user.id, 'name': self.user.username})


@login_required
def players(request):
    ids = get_online_users()
    users = User.objects.filter(pk__in=ids).exclude(pk=request.user.id)
    return render(request, 'game/players.html', {'online_users': users})


def change_lang(request, lang):
    response = HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    cookie_name = getattr(settings, 'LANGUAGE_COOKIE_NAME')
    response.set_cookie(cookie_name, lang)
    return response
