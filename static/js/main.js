$(function(){
    "use strict";

    var socket = io.connect("/game");

    var statuses = {
        idle: 'idle',
        my_move: 'my_move',
        wait_for_opp: 'wait_for_opp',
        start: 'start',
        replay_possible: 'replay_possible',
        replay_confirm: 'replay_confirm',
        got_invite: 'got_invite',
        wait_for_accept_invite: 'wait_for_accept_invite',
        game_stopped: 'game_stopped'
    };

    var status = statuses.idle,
        game_id,
        me,
        opp;

    // helpers

    var resetGame = function(){
        $('.gameboard div').html('');
        game_id = null;
//        me = null;
        opp = null;
    };

    var makeMove = function(move, mark){
        var square = $('#' + move.x + move.y);
        if (square.html().trim() == ''){
            square.html(mark);
            return true;
        }
        return false;
    };

    var message = function(msg, alertClass){
        $('.message').html(msg)
            .removeClass('alert-success alert-info alert-warning alert-danger')
            .addClass(alertClass);
    };

    var showPlayers = function(){
        $('.players-list').removeClass('hide');
        $('.game-container').addClass('hide');
    };

    var showBoard = function(){
        $('.players-list').addClass('hide');
        $('.game-container').removeClass('hide');
    };

    var showButtons = function(groupClass){
        $('.btns').addClass('hide');
        if (groupClass) {
            $(groupClass).removeClass('hide');
        }
    };

    // socket events

    socket.emit('subscribe', '1');

    setInterval(function(){
        socket.emit('online', '1');
    }, 1000);

    // when new user goes online
    socket.on('user_online', function(user){
        if ($('#user' + user.id).length === 0) {
            var li = $('<li class="list-group-item" id="user' + user.id + '">' + user.name + '</li>'),
                button = $('<button class="btn btn-success invite">' + gettext('Invite') + '</button>');

            button.attr('data-id', user.id);
            li.prepend(button);
            $('#online_users').prepend(li);
        }

    });

    // when new user goes offline
    socket.on('user_offline', function(user){
        if (status !== statuses.idle && status !== statuses.game_stopped && user.id === opp.id) {
            status = statuses.game_stopped;
            message(user.name + gettext(' disconnected. Please, start a new game!'), 'alert-danger');
            showButtons('.close-buttons');
        }
        $('#user' + user.id).remove();
    });

    // when opp stops active game
    socket.on('stop_game', function(user){
        message(user.name + gettext(' gave up. You won!'), 'alert-success');
        status = statuses.game_stopped;
        showButtons('.close-buttons');
    });

    // when someone invites you
    socket.on('invite', function(user){
        if (status !== statuses.idle){
            socket.emit('decline_invite', user.id, "I'm currently busy");
            return;
        }
        status = statuses.got_invite;
        resetGame();
        opp = user;
        showBoard();
        showButtons('.invite-buttons');
        message(gettext('You got invite from ') + user.name, 'alert-success');
    });

    // when someone declines your invite
    socket.on('decline_invite', function(user){
        status = statuses.game_stopped;
        message(gettext('Your invite was declined: ') + gettext(user.reason), 'alert-danger');
        showButtons('.close-buttons');
    });

    // when someone invites you and then cancels his invite
    socket.on('cancel_invite', function(){
        status = statuses.game_stopped;
        message(gettext('Invite was canceled'), 'alert-danger');
        showButtons('.close-buttons');
    });

    // when your opponent wants to play again
    socket.on('replay', function(user){
        if (status == statuses.replay_possible){
            status = statuses.replay_confirm;
            message(user.name + gettext(' wants to play again!'), 'alert-success');
        }
    });

    // when opponent refuses to play again
    socket.on('decline_replay', function(user){
        status = statuses.game_stopped;
        message(user.name + gettext(" doesn't want to play again!"), 'alert-danger');
        showButtons('.close-buttons');
    });

    socket.on('game_start', function(game){
        me = game.me;
        opp = game.opp;
        game_id = game.id;
        status = statuses.start;
        message(gettext('Game started'), 'alert-warning');
        showButtons('.stop-buttons');
    });

    socket.on('move', function(move){
        status = statuses.my_move;
        if (move) {
            makeMove(move, opp.mark);
        }
        message(gettext('Your move'), 'alert-success');
    });

    socket.on('game_end', function(result){
        status = statuses.replay_possible;
        if (result == 'won'){
            message(gettext('You won :)'), 'alert-info');
        } else if (result == 'lost'){
            message(gettext('You lost :('), 'alert-info');
        } else {
            message(gettext("It's a draw!"), 'alert-info');
        }
        showButtons('.replay-buttons');
    });

    // UI events

    // invite user
    $('#online_users').on('click', '.invite', function(){
        resetGame();
        opp = {'id': parseInt($(this).attr('data-id'))};
        socket.emit('invite', opp.id);
        status = statuses.wait_for_accept_invite;
        showBoard();
        showButtons('.cancel-invite-buttons');
        message(gettext('Waiting for user to accept invite.'), 'alert-warning');
    });

    // move
    $('.gameboard').on('click', 'div', function(){
        var move = {x: this.id[0], y: this.id[1]};

        if (status !== statuses.my_move){
            return;
        }
        if (makeMove(move, me.mark)){
            status = statuses.wait_for_opp;
            socket.emit('move', move, game_id);
            message(gettext('Wait for opponent to move'), 'alert-warning');
        } else {
            message(gettext('Incorrect move: try again'), 'alert-danger');
        }
    });

    // cancel own invite
    $('.cancel-invite-buttons .cancel').click(function(){
        if (status == statuses.wait_for_accept_invite){
            socket.emit('cancel_invite', opp.id);
            status = statuses.idle;
            showPlayers();
        }
    });

    // accept invite
    $('.invite-buttons .accept').click(function(){
        if (status == statuses.got_invite){
            socket.emit('accept_invite', opp.id);
            showButtons(null);
        }
    });

    // decline invite
    $('.invite-buttons .decline').click(function(){
        if (status == statuses.got_invite){
            socket.emit('decline_invite', opp.id, "I don't want to play now!");
            status = statuses.idle;
            showPlayers();
        }
    });

    $('.close-buttons .back').click(function() {
        status = statuses.idle;
        showPlayers();
    });


    // stop active game and go to players list
    $('.stop-buttons .stop').click(function() {
        status = statuses.idle;
        socket.emit('stop_game', opp.id);
        showPlayers();
    });

    // play again
    $('.replay-buttons .replay').click(function(){
        if (status == statuses.replay_possible){
            // we propose to play again first
            socket.emit('replay', opp.id);
            message(gettext('Wait for opponent to accept invite'), 'alert-warning');
        } else if (status == statuses.replay_confirm){
            // our opponent already proposed to play again
            socket.emit('accept_invite', opp.id);
        }
        showButtons(null);
        resetGame();
    });

    // refuse from playing again
    $('.replay-buttons .replay-cancel').click(function(){
        if (status == statuses.replay_possible || status == statuses.replay_confirm){
            socket.emit('decline_replay', opp.id);
            status = statuses.idle;
            showPlayers();
        }
    });
});
