# Tic-Tac-Toe
test assignment

## Installation in Ubuntu:

### Setup environment:
git clone git@bitbucket.org:kirius/tic-tac-toe.git

mkvirtualenv tic

cd tic-tac-toe

pip install -r requirements.txt

### Initialize app:
./manage.py migrate

./manage.py compilemessages

### Start redis in separate window:
redis-server

### Start server:
./manage.py runserver_socketio

Visit http://127.0.0.1:8000/ in a browser